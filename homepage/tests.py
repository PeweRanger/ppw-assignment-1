from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .views import *
from .apps import *

class HomeUnitTest(TestCase):
    def test_home_url_is_exist(self):
        respone = Client().get('/home/')
        self.assertEqual(respone.status_code, 200)

    def test_home_url_isnot_exist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)
