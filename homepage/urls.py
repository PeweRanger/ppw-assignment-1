from django.urls import path
from .views import home, logoutuser

app_name = 'homepage'

urlpatterns = [
    path('', home, name='home'),
    path('/logout/', logoutuser, name='logout'),
]
