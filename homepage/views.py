from django.shortcuts import render, redirect
from programs.models import ProgramDonasi
from registerpage.models import Donatur
# Create your views here.

#response = {}
def home(request):
        response = get_response()
        program = ProgramDonasi.objects.all()
        donatur = Donatur.objects.count()
        response['programlist'] = program
        response['JumlahDonatur'] = donatur
        return render(request, 'home.html', response)


def get_response():
    judul_1 = 'Mereka saat ini sangat membutuhkan uluran tanganmu,'
    judul_2 = 'Ayo bantu mereka !'
    nama = 'Sinar Perak'
    desc = 'Situs galang dana dan berdonasi secara terbuka bagi yang memerlukan'
    connect = 'Connect'
    email = ' sinarperak@gmail.com'
    cp = ' 081318405772 / Jasmine'
    response = {'judul_1':judul_1, 'judul_2':judul_2, 'nama':nama, 'desc':desc, 'connect':connect, 'email':email, 'cp': cp}
    return response


def logoutuser(request):
	logout(request)
	print('hehe')
	return HttpResponseRedirect('/landingpage/')
