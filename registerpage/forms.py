from django import forms

class RegisterForm(forms.Form):
    attrs = {'class':'form-control'}
    attrs_date = {'type':'date'}
    attrs_time = {'type':'time'}
    name = forms.CharField(label='Your Name', max_length=50, required=True,
        widget=forms.TextInput(attrs=attrs))
    birthdate = forms.DateField(label='Birtday Date', required=False,
        widget=forms.DateInput(attrs={**attrs, **attrs_date}))
    email = forms.CharField(label='Email', max_length=100, required=True,
        widget=forms.EmailInput(attrs=attrs))
    password = forms.CharField(label='Password', max_length=75, required=True,
        widget=forms.PasswordInput(attrs=attrs))
