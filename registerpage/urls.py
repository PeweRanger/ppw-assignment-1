from django.conf.urls import url
from django.urls import path

from . import views

app_name = "registerpage"

urlpatterns = [
    path('', views.show_and_process_register_page, name='registerpage'),
    # path('process/', views.register_form_process, name='register_form_process'),
    # path('process/404/', views.show_404,name='show404'),
    # path('landing/', views.index, name='index'),
    # path('process/', views.submit_message, name = 'form_process'),
    # path('profile/', views.showprofile, name = 'profile'),
]
