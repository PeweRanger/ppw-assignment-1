from django.test import TestCase, Client
from django.http import HttpRequest
from django.test import SimpleTestCase
from django.urls import resolve, reverse
from django.apps import apps
from . import views
from .apps import RegisterpageConfig
from .models import Donatur
from .forms import RegisterForm

class AppTests(TestCase):
    def test_apps(self):
        self.assertEqual(RegisterpageConfig.name, 'registerpage')
        self.assertEqual(apps.get_app_config('registerpage').name, 'registerpage')

class RegisterPageTest(TestCase):
    def test_register_page_status_code(self):
        response = Client().get('/register/')
        self.assertEquals(response.status_code,200)
    
    def test_register_page_using_show_register_page_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, views.show_and_process_register_page)
    
    def test_have_register_page_message(self):
        response = Client().get('/register/')
        self.assertContains(response,'Pendaftaran')
    
    def test_register_page_view_uses_correct_template(self):
        response = self.client.get(reverse('registerpage:registerpage'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'registerpage.html')

    # def test_view_url_by_form_process_name(self):
    #     response = self.client.get(reverse('registerpage:register_form_process'))
    #     self.assertEquals(response.status_code, 302)
    
    # def test_register_form_process_using_form_process_func(self):
    #     found = resolve('/register/process/')
    #     self.assertEqual(found.func, views.register_form_process)
    
class DonaturModelTest(TestCase):
    def test_models_created(self):
        Donatur.objects.create(name='Eminem', email='hoho@nothing.com', birthdate='2001-12-2', password='rapgod')
        counting_all_available_donatur = Donatur.objects.all().count()
        self.assertEqual(counting_all_available_donatur, 1)
        string=Donatur.objects.get(email = 'hoho@nothing.com')
        self.assertEqual('hoho@nothing.com', str(string))

    def test_post_donatur_content(self):
        response = self.client.post('/register/', {'name': 'Eminem', 'email':'hoho@nothing.com', 'birthdate':'2001-12-2', 'password':'rapgod'})
        self.assertEqual(Donatur.objects.count(), 1)
        new_donatur = Donatur.objects.first()
        self.assertEqual(new_donatur.name, 'Eminem')
    
    # def test_double_email_exception(self):
    #     response = self.client.post('/register/', {'name': 'Eminem', 'email':'hoho@nothing.com', 'birthdate':'2001-12-2', 'password':'rapgod'})
    #     response2 = self.client.post('/register/', {'name': 'Eminem', 'email':'hoho@nothing.com', 'birthdate':'2001-12-2', 'password':'rapgod'})
    #     self.assertContains(response2,'Email is already taken')
        
class RegisterFormTest(TestCase):
   def test_forms_valid(self):
        form_data = {'name': 'Eminem', 'email':'hoho@nothing.com', 'birthdate':'2001-12-2', 'password':'rapgod'}
        form = RegisterForm(data=form_data)
        self.assertTrue(form.is_valid())
        donatur = Donatur()
        donatur.name = form.cleaned_data['name']
        donatur.birthdate = form.cleaned_data['birthdate']
        donatur.save()
        self.assertEqual(donatur.name, "Eminem")
        

# Create your tests here.
