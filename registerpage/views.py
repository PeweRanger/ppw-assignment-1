from django.shortcuts import render, redirect
# from django.http import HttpRedirect
from .forms import RegisterForm
from .models import Donatur

response = {}

def show_and_process_register_page(request):
    response['registform'] = RegisterForm()
    response['errormessage'] = ''
    form = RegisterForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['birthdate'] = request.POST['birthdate']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        donatur = Donatur(
            name = response['name'],
            email = response['email'],
            birthdate = response['birthdate'],
            password = response['password']
            )
        try:
            donatur.save()
        except Exception:
            response['errormessage'] = 'This email is already taken'
            return render(request,'registerpage.html', response)
            #/register #/404 #/404 register/404
        return redirect('/home/')
    return render(request, 'registerpage.html', response)


# def show_register_page(request):
#     response['registform'] = RegisterForm()
#     return render(request, 'registerpage.html',response)

# def register_form_process(request):
#     form = RegisterForm(request.POST or None)
#     if(request.method == 'POST' and form.is_valid()):
#         response['name'] = request.POST['name']
#         response['birthdate'] = request.POST['birthdate']
#         response['email'] = request.POST['email']
#         response['password'] = request.POST['password']
#         donatur = Donatur(
#             name = response['name'],
#             email = response['email'],
#             birthdate = response['birthdate'],
#             password = response['password']
#             )
#         try:
#             donatur.save()
#         except Exception:
#             response['errormessage'] = 'This email is already taken'
#             return render(request,'registerpage.html', response)
#             #/register #/404 #/404 register/404
#         return redirect('/home/')
#     else:
#         return r edirect('/')

# def show_404(request):
#     return render(request, '404.html',response)
# Create your views here.
