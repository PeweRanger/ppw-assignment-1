from django.db import models

class Donatur(models.Model):
    name = models.CharField(max_length=100)
    birthdate = models.DateTimeField()
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.email
# Create your models here.
