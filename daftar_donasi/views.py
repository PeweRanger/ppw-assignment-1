from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import Donasi
import json
import requests
# Create your views here.
def daftar_donasi_api(request):
    response = { "donatur": list() }
    transaksi = Donasi.objects.all()
    daftar_donatur = dict()
    for i in transaksi:
        if i.email_donatur in daftar_donatur:
            if i.nama_program in daftar_donatur[i.email_donatur]["program"]:
                daftar_donatur[i.email_donatur]["program"][i.nama_program] += i.jumlah_donasi
            else:
                daftar_donatur[i.email_donatur]["program"][i.nama_program] = i.jumlah_donasi
        else:
            daftar_donatur[i.email_donatur] = dict()
            daftar_donatur[i.email_donatur]["nama"] = i.nama_donatur
            daftar_donatur[i.email_donatur]["program"] = dict()
            daftar_donatur[i.email_donatur]["program"][i.nama_program] = i.jumlah_donasi

    for i in daftar_donatur:
        response["donatur"].append({
            "nama": daftar_donatur[i]["nama"],
            "email": i,
            "totalDonasi": sum([daftar_donatur[i]["program"][program] for program in daftar_donatur[i]["program"]]),
            "daftarDonasi": [{  "namaProgram": program, 
                                "jumlahDonasi": daftar_donatur[i]["program"][program]
                            } for program in daftar_donatur[i]["program"]]
        })
    return JsonResponse(response)


def daftar_donasi(request):
    if(not request.user.is_authenticated):
        return HttpResponseRedirect('/')
    return render(request, "daftar_donasi.html")

def daftar_donasi_user(request):
    if(not request.user.is_authenticated):
        return HttpResponseRedirect('/')
    api_data = daftar_donasi_api(request)
    json_data = json.loads(api_data.content)
    response = dict()
    for donatur in json_data["donatur"]:
        if donatur["email"] == request.user.email:
            response["user"] = donatur
            return JsonResponse(response)
    return JsonResponse({ "user": { "totalDonasi": 0 } })