from django.urls import path
from .views import daftar_donasi, daftar_donasi_api, daftar_donasi_user

app_name = 'daftar_donasi'

urlpatterns = [
    path('', daftar_donasi, name='daftar_donasi'),
    path('api', daftar_donasi_api, name='daftar_donasi_api'),
    path('user', daftar_donasi_user, name='daftar_donasi_user'),
]
