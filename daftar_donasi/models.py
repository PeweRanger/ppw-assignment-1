from django.contrib.auth.models import AbstractUser
from django.db import models
from programs.models import ProgramDonasi
# Create your models here.

class Donasi(models.Model):
    nama_donatur = models.CharField(max_length=50, default="tes")
    email_donatur = models.CharField(max_length=50)
    nama_program = models.CharField(max_length=50)
    jumlah_donasi = models.IntegerField()