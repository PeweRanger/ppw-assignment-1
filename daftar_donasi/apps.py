from django.apps import AppConfig


class DaftarDonasiConfig(AppConfig):
    name = 'daftar_donasi'
