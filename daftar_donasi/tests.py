from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User
from .models import Donasi
from .views import daftar_donasi, daftar_donasi_api, daftar_donasi_user
import json

class DaftarDonasiAPIUnitTest(TestCase):

    def setUp(self):
        user = User.objects.create(username="test", email="donatur1@gmail.com")
        user.set_password('12345')
        user.save()
        logged_in = self.client.login(username='test', password='12345')

        donasi = Donasi.objects.create(
            nama_donatur="donatur1",
            email_donatur="donatur1@gmail.com",
            nama_program="program1",
            jumlah_donasi=12345
        )
        donasi.save()
        donasi = Donasi.objects.create(
            nama_donatur="donatur1",
            email_donatur="donatur1@gmail.com",
            nama_program="program1",
            jumlah_donasi=54321
        )
        donasi.save()
        donasi = Donasi.objects.create(
            nama_donatur="donatur1",
            email_donatur="donatur1@gmail.com",
            nama_program="program2",
            jumlah_donasi=12345
        )
        donasi.save()

    def test_daftar_donasi_is_exist(self):
        response = self.client.get('/daftar_donasi/')
        found = resolve('/daftar_donasi/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, daftar_donasi)

    def test_daftar_donasi_api_is_exist(self):
        response = self.client.get('/daftar_donasi/api')
        found = resolve('/daftar_donasi/api')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, daftar_donasi_api)

    def test_daftar_donasi_user_is_exist(self):
        response = self.client.get('/daftar_donasi/user')
        found = resolve('/daftar_donasi/user')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, daftar_donasi_user)

    def test_user_not_logged_in_and_redirect_to_landing_page(self):
        new_client = Client()
        response1 = new_client.get('/daftar_donasi/')
        response2 = new_client.get('/daftar_donasi/user')
        self.assertEqual(response1.status_code, 302)
        self.assertEqual(response2.status_code, 302)

    def test_can_save_model_donasi(self):
        prev = Donasi.objects.all().count()
        donasi = Donasi.objects.create(
            nama_donatur="donatur1",
            email_donatur="donatur1@gmail.com",
            nama_program="program3",
            jumlah_donasi=10000
        )
        donasi.save()
        self.assertEqual(Donasi.objects.all().count() - prev, 1)

    def test_new_user_donation(self):
        new_user = User.objects.create(username="new_user", email="donatur2@gmail.com")
        new_user.set_password('tesset')
        new_user.save()

        new_client = Client()
        logged_in = new_client.login(username='new_user', password='tesset')
        json_response = new_client.get('/daftar_donasi/user').content
        total_donation_of_new_user = json.loads(json_response)['user']['totalDonasi']
        self.assertEqual(total_donation_of_new_user, 0)

    def test_success_adding_total_donation(self):
        json_response = self.client.get('/daftar_donasi/user').content
        prev_total_donation = json.loads(json_response)['user']['totalDonasi']

        now = 15000
        donasi = Donasi.objects.create(
            nama_donatur="donatur1",
            email_donatur="donatur1@gmail.com",
            nama_program="program3",
            jumlah_donasi=now,
        )
        donasi.save()

        json_response = self.client.get('/daftar_donasi/user').content
        total_donation = json.loads(json_response)['user']['totalDonasi']
        self.assertEqual(total_donation, now + prev_total_donation)