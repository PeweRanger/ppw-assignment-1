# PPW-Assignment-1

# Nama Anggota Kelompok
Bimo Iman Smartadi - 1706039780
Jasmine Nur Safiera - 1706024841
Mohammad Adli Daffa Wirapanatayudha - 1706984663
Muhammad Nadhirsyah indra - 1706039383

# Link Herokuapp
Link: [http://pepewrangers.herokuapp.com/](http://pepewrangers.herokuapp.com/)

# Pipeline Status
[![pipeline status](https://gitlab.com/PeweRanger/ppw-assignment-1/badges/master/pipeline.svg)](https://gitlab.com/PeweRanger/ppw-assignment-1/commits/master)

# Coverage Report
[![coverage report](https://gitlab.com/PeweRanger/ppw-assignment-1/badges/master/coverage.svg)](https://gitlab.com/PeweRanger/ppw-assignment-1/commits/master)

## Checklist

### Komponen

_Admin Page for project (?) Is it possible?_

1. Landing Page
    1. [x] Buat App
    2. [x] Desain HTML CSS
    3. [ ] Redirect Function ke halaman-halaman kebutuhan _Minimal ke login page_
2. Register Page
    1. [ ] Buat App
    2. [ ] Buat Models Donatur
        - Nama
        - Email [unique]
        - Tanggal Lahir
        - Password
        - Donated Program{Terhubung dengan models DonationProgram}
    3. [ ] Buat Form integrasi dengan models donatur (User-fill)
        - Nama
        - Email
        - Tanggal Lahir
        - Password
    4. Models Donatur integrasi dengan models DonationProgram
3. Home Page
    1. [ ] Buat App
    2. [ ] List program and News _1 dulu [Integrasi dengan DonationProgram]_
    3. [ ] {{Super Additionals}} Ada angka yang menunjukkan sudah berapa yang register
4. Program and News Page
    1. [x] Buat App
    2. [x] Reusable HTML template _untuk 1 program dulu_
    3. [x] Buat Models DonationProgram (Admin-fill) _Berisi 1 program dulu_
    4. [x] Buat Form untuk program tersebut
        - Nama
        - Email
        - Jumlah Donasi
        - Check box nama
    5. [ ] Integrasi dengan Models Donatur untuk menampilkan nama donatur
