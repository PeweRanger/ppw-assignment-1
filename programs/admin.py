from django.contrib import admin
from .models import ProgramDonatur, ProgramDonasi

# Register your models here.
admin.site.register(ProgramDonasi)
admin.site.register(ProgramDonatur)