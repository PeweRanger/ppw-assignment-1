# Generated by Django 2.1.1 on 2018-10-16 14:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProgramDonasi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=255)),
                ('url_image', models.CharField(max_length=255)),
                ('target', models.CharField(max_length=255)),
                ('desc', models.TextField()),
                ('terkumpul', models.IntegerField()),
            ],
        ),
    ]
