# Generated by Django 2.1.1 on 2018-12-08 16:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0004_programdonasi_user_donatur'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='programdonasi',
            name='user_donatur',
        ),
    ]
