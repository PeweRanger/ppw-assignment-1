from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import DonationForm
from .models import ProgramDonasi, ProgramDonatur
from daftar_donasi.models import Donasi
from registerpage.models import Donatur

# Create your views here.
def index(request, id):
    program = ProgramDonasi.objects.get(id=id)
    donaturs = program.donaturs.all()
    donation_form = DonationForm
    response = {'donation_form': donation_form, 'program': program, 'donaturs': donaturs[::-1],
                    'count': donaturs.count()}
    return render(request, 'programs.html', response)

def donation(request, id):
    form = DonationForm(request.POST or None)
    form_email = request.POST['email']

    if(request.method == 'POST'
        and form.is_valid()
        and request.POST['donasi'].isdigit()
        and (Donatur.objects.filter(email=form_email).exists() or request.user.is_authenticated)):

        program = ProgramDonasi.objects.get(id=id)
        donasi = int(request.POST['donasi'])

        if('anonymous' not in request.POST):
            nama = request.POST['nama']
        else:
            nama = 'Fulan bin Fulan'

        donatur = ProgramDonatur(nama=nama, donasi=donasi)
        donatur.save()
        program.donaturs.add(donatur)
        program.terkumpul += donasi
        program.save()

        transaksi_donasi = Donasi(  nama_donatur=request.user.get_full_name(),
                                    email_donatur=request.user.email,
                                    nama_program=program.nama,
                                    jumlah_donasi=donasi)
        transaksi_donasi.save()

        return HttpResponseRedirect('/programs/'+str(id))
    else:
        return HttpResponseRedirect('/programs/'+str(id))
