from django.urls import path
from .views import donation, index

app_name = 'programs'

urlpatterns = [
    path('<int:id>/', index, name='program'),
    path('donation/<int:id>', donation, name='donation')
]