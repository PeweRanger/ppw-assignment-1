from django.apps import apps
from django.http import HttpRequest
from django.test import Client, TestCase
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .apps import ProgramsConfig
from .models import ProgramDonatur, ProgramDonasi
from .views import donation, index
from registerpage.models import Donatur

class AppTest(TestCase):
    def test_apps(self):
        self.assertEqual(ProgramsConfig.name, 'programs')
        self.assertEqual(apps.get_app_config('programs').name, 'programs')

class ProgramsPageTest(TestCase):
    def setUp(self):
        user = User.objects.create(username="user1", email="user1@gmail.com")
        user.set_password('user1fortest')
        user.save()
        logged_in = self.client.login(username='user1', password='user1fortest')

    def create_database(self, nama='', url='', target='', desc='', total=0):
        self.program = ProgramDonasi(
            nama=nama,
            url_image=url,
            target=target,
            desc=desc,
            terkumpul=total
        )
        self.donatur = Donatur(
            name='tes',
            email='tes@mail.com',
            birthdate='2001-12-2',
            password='rapgod'
        )
        self.program.save()
        self.donatur.save()

    def test_programs_using_the_right_template(self):
        self.create_database()
        response = self.client.get('/programs/1/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'programs.html')

    def test_programs_contains_desc(self):
        self.create_database(nama='Fabian tidak sendirian !')
        response = Client().get('/programs/1/')
        self.assertContains(response,'Fabian tidak sendirian !')

    def test_programs_using_hello_func(self):
        self.create_database()
        found = resolve('/programs/1/')
        self.assertEqual(found.func, index)

    def test_model_for_program_donasi(self):
        new_program = ProgramDonasi.objects.create(
            nama='tes',
            url_image='tes.jpg',
            target='1234',
            desc='tes 123',
            terkumpul=123,
        )
        self.assertEqual(ProgramDonasi.objects.all().count(), 1)

    def test_model_for_donatur(self):
        new_donatur = ProgramDonatur.objects.create(
            nama='tes',
            donasi=123,
        )
        self.assertEqual(ProgramDonatur.objects.all().count(), 1)

    def test_can_save_donatur_model_using_form(self):
        self.create_database()
        response = self.client.post('/programs/donation/1',
            {
            'nama': 'tes',
            'email': 'tes@mail.com',
            'donasi': '123',
            }
        )
        self.assertEquals(response.status_code, 302)
        html_response = self.client.get('/programs/1/').content.decode('utf8')
        self.assertIn('tes', html_response)
        self.assertIn('123', html_response)

    def test_form_can_save_donatur_name_as_anonymous(self):
        self.create_database()
        response = self.client.post('/programs/donation/1',
            {
            'nama': 'tes',
            'email': 'tes@mail.com',
            'donasi': '123',
            'anonymous': True,
            }
        )
        html_response = self.client.get('/programs/1/').content.decode('utf8')
        self.assertIn('Fulan bin Fulan', html_response)

    def test_form_can_validate_donation_number(self):
        self.create_database()
        response = self.client.post('/programs/donation/1',
            {
            'nama': 'bug',
            'email': 'tes@mail.com',
            'donasi': '-123',
            }
        )
        html_response = self.client.get('/programs/1/').content.decode('utf8')
        self.assertNotIn('bug', html_response)
        self.assertNotIn('-123abc', html_response)

    def test_model_program_donasi_has_total_donation_and_donator(self):
        self.create_database()
        response = self.client.post('/programs/donation/1',
            {
            'nama': 'abc',
            'email': 'tes@mail.com',
            'donasi': '123',
            }
        )
        response = self.client.post('/programs/donation/1',
            {
            'nama': 'def',
            'email': 'tes@mail.com',
            'donasi': '321',
            }
        )
        total_donation = ProgramDonasi.objects.get(id=1).terkumpul
        total_donatur = ProgramDonasi.objects.get(id=1).donaturs.all().count()
        self.assertEquals(total_donation, 444)
        self.assertEquals(total_donatur, 2)
