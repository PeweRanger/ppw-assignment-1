from django import forms

class DonationForm(forms.Form):
    attrs = {
        'class': 'form-control',
    }
    nama = forms.CharField(label='Nama', max_length=255, required=True, widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label='Email', max_length=255, required=True, widget=forms.EmailInput(attrs=attrs))
    donasi_attrs = {
        'class': 'form-control',
        'pattern': '[0-9]{1,9}',
    }
    donasi = forms.CharField(label='Jumlah Donasi', max_length=9, required=True, widget=forms.TextInput(attrs=donasi_attrs))
    anonymous = forms.BooleanField(label='', widget=forms.CheckboxInput(), required=False)