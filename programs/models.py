from django.db import models

# Create your models here.
class ProgramDonatur(models.Model):
    nama = models.CharField(max_length=255)
    donasi = models.IntegerField()
    
    def __str__(self):
        return self.nama

class ProgramDonasi(models.Model):
    nama = models.CharField(max_length=255)
    url_image = models.CharField(max_length=255)
    target = models.CharField(max_length=255)
    desc = models.TextField()
    terkumpul = models.IntegerField()
    donaturs = models.ManyToManyField(ProgramDonatur, blank=True)
    
    def __str__(self):
        return self.nama