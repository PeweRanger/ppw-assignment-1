"""pepewrangers URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic.base import RedirectView
import registerpage.urls as registerpageUrls
import landingpage.views as landingview
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='landingpage/'), name='landingpage'),
    path('hello/', include('hello_world.urls')),
    path('landingpage/', include('landingpage.urls')),
    path('register/', include('registerpage.urls'), name='registerpage'),
    path('logout/', landingview.logoutuser, name = 'logout'),
    path('home/', include('homepage.urls')),
    path('programs/', include('programs.urls')),
    path('daftar_donasi/', include('daftar_donasi.urls'), name='daftar_donasi'),
    re_path(r'^auth/', include('social_django.urls', namespace='social')),
    path('testimonipage/', include('testimonipage.urls')),

]
