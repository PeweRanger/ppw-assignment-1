$(document).ready(function() {
    $.ajax({
        url: "/daftar_donasi/user",
        success: function(response) {
            $("#total_donasi").html("<h2>Total donasi anda: Rp." + response.user.totalDonasi + "</h2>");
            if(response.user.totalDonasi > 0) createTable(response.user.daftarDonasi);
            else $("#table_donasi").html("<h2>Belum ada donasi.</h2>")
        }
    })
    function createTable(daftarDonasi) {
        var output = "<br><table class='table table-primary table-hover table-bordered'><thead style='background-color:#ffc107e0'><tr><th>Nama Program</th><th>Jumlah Donasi</th></tr></thead><tbody style='background-color:white;'>";

        for(var i = 0; i < daftarDonasi.length; ++i) {
            output += "<tr><td>" + daftarDonasi[i].namaProgram + "</td><td>" + daftarDonasi[i].jumlahDonasi + "</td></tr>";
        }
        output += "</tbody></table>";
        $("#table_donasi").html(output);
    }
});