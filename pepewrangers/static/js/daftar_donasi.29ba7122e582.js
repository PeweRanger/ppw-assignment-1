$(document).ready(function() {
    var saveHtml = "";
    $("#daftar_donasi_btn").click(function() {
        $.ajax( {
            url: "/daftar_donasi",
            success: function(response) {
                if(saveHtml === "") {
                    var output = "<table class='table daftar_donasi'><thead><th scope='col'>Nomor</th><th scope='col'>Nama Donatur</th><th scope='col'>Jumlah Donasi</th></tr></thead><tbody>";
                    var daftar_donatur = res.daftar_donatur;
                    for(var i = 0; i < daftar_donatur.length; ++i) {
                      output += "<tr><th scope='row'>" + (i+1) + "</th><td>" + daftar_donatur[i].nama + "</td><td>" + daftar_donatur[i].donasi + "</td><tr>";
                    }
                    output += "</tbody></table>";
                    saveHtml = $("#daftar_donasi").html();
                    $("#daftar_donasi").html(output);
                    $('#daftar_donasi_btn').html('Back');
                } else {
                    $("#daftar_donasi").html(saveHtml);
                    saveHtml = "";
                    $('#daftar_donasi_btn').html('Daftar Donasi');
                }
            }
        })
    })
});