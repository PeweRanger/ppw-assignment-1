from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views

class landingpageUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/landingpage/')
		self.assertEqual(response.status_code,200)

	def test_url_using_landingpage_template(self):
		response = Client().get('/landingpage/')
		self.assertTemplateUsed(response, 'landingpage.html')

	def test_urls_using_display_landingpage_func(self):
		found = resolve('/landingpage/')
		self.assertEqual(found.func, views.landingpage)
