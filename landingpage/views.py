from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout

# Create your views here.
def landingpage(request):
	return render(request, "landingpage.html")

def logoutuser(request):
	logout(request)
	print('hehe')
	return HttpResponseRedirect('/landingpage/')