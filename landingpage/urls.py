from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = "landingpage"

urlpatterns = [
	url('', views.landingpage , name='landingpage'),
	url('logout', views.logoutuser, name='logout'),
]