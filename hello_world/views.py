from django.shortcuts import render

# Create your views here.
def hello_world(request):
    response = {'text':'Hello World!'}
    return render(request, 'hello_world.html', response)