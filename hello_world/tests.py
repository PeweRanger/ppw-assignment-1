from django.test import TestCase, Client
from django.http import HttpRequest
from django.test import SimpleTestCase
from django.urls import resolve, reverse
from django.apps import apps
from .apps import HelloWorldConfig
from . import views

class AppTest(TestCase):
    def test_apps(self):
        self.assertEqual(HelloWorldConfig.name, 'hello_world')
        self.assertEqual(apps.get_app_config('hello_world').name, 'hello_world')

class HelloWorldPageTest(TestCase):
    def test_hello_world_using_the_right_template(self):
        response = self.client.get(reverse('hello_world:hello'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'hello_world.html')

    def test_hello_world_contains_hello(self):
        response = Client().get('/hello/')
        self.assertContains(response,'Hello')

    def test_hello_world_using_hello_func(self):
        found = resolve('/hello/')
        self.assertEqual(found.func, views.hello_world)
# Create your tests here.
