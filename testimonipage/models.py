from django.db import models
from datetime import datetime,date
from django.utils import timezone


class Testimoni(models.Model):
	username = models.CharField(max_length=100)
	comment = models.CharField(max_length=500, null=True)
	time = models.DateTimeField(auto_now_add = True, null = True, blank = True)

