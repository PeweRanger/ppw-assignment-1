from django.conf.urls import url
from django.urls import include, path
from .views import about, addComment

app_name = "about"

urlpatterns = [
	path('', about , name='about'),
	path('addComment', addComment , name='addComment'),
]