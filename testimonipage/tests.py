from django.test import TestCase
from django.test import Client
from django.urls import resolve

class testimonipage(TestCase):
    def test_testimonipage_url_is_exist(self):
        response = Client().get('/testimonipage/')
        self.assertEqual(response.status_code, 200)
        text_response = response.content.decode('utf8')
        self.assertIn("Founder", text_response)
