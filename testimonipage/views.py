from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from .models import Testimoni
from .forms import Comment

def about(request):
	response = {}

	form = Comment(request.POST or None)
	CommentUser = Testimoni.objects.all()
	response = {
		"form" : form, 
		"CommentUser" : CommentUser,
	}
	return render(request, "about.html", response)

def addComment(request):
	form = Comment(request.POST)
	print('added')

	if (request.method == 'POST'):
		print('posted')
		if(form.is_valid()):
			print('valid')
			message = request.POST["testimoni_komentar"]
			testi = Testimoni(comment = message, username = "User")
			testi.save()
	return redirect('about:about') 

