from django.apps import AppConfig


class TestimonipageConfig(AppConfig):
    name = 'testimonipage'
